from django.contrib import admin

from .models import MeetRequest

# Register your models here.
admin.site.register(MeetRequest)
