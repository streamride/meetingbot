from django.http import JsonResponse
from django.shortcuts import render
import json
from django.views.decorators.csrf import csrf_exempt

from base.models import MeetRequest


@csrf_exempt
def post_meeting_request(request):
    data = json.loads(request.body)
    meetin = MeetRequest()
    meetin.name = data['name']
    meetin.address = data['address']
    meetin.city = data['city']
    meetin.phone = data['phone']
    meetin.cigarette = data['cigarette']
    meetin.day = data['day']
    meetin.time = data['time']
    meetin.save()
    return JsonResponse({"id": meetin.id})
