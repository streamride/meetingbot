from django.contrib import admin
from django.urls import path

from base import views

urlpatterns = [
    path('meeting/', views.post_meeting_request, name='set_request')
]
