from django.db import models


class MeetRequest(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    phone = models.TextField()
    city = models.CharField(max_length=200)
    cigarette = models.CharField(max_length=100)
    day = models.CharField(max_length=100)
    time = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name} - {self.phone}"
